# Game of the Rope #

This is the client-server upgrade of a concurrent solution of the classic Game of the Problem. Communication between clients and servers is implemented via a message/response mechanism over TCP channels created between a client and a server. 
The clients are represented by a referee and two teams (each of the composed by a coach and 5 contestants). They create communication channels with each of the three servers, which provide operations over shared regions Bench, Playground and Referee Site. These three servers behave as clients with the General Repository server. 
This project is the second of three projects from the Distributed Systems course, from the 4th year of the Integrated Masters in Computers and Telematics Engineering (University of Aveiro).

### Main keywords ###
* Java
* Concurrency
* Explicit monitors
* Sockets
* Parallelsm
* Communication channels

### Running ###

* Just run the deployment script run.sh

### Owners ###

The entire solution was developped by Rui Espinha Ribeiro ([Espinha](https://bitbucket.org/Espinha)) and David Silva ([dmpasilva](https://bitbucket.org/dmpasilva)).