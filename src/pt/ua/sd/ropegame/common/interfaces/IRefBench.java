package pt.ua.sd.ropegame.common.interfaces;


public interface IRefBench {

    void callTrial();

    void notifyContestantsMatchIsOver();
}
