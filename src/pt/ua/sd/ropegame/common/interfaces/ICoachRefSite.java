package pt.ua.sd.ropegame.common.interfaces;

public interface ICoachRefSite {

    void informReferee(int teamID);

}
