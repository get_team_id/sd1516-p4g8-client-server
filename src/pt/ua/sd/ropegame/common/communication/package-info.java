/**
 * This package contains all classes required for handling communication and messaging.
 */
package pt.ua.sd.ropegame.common.communication;