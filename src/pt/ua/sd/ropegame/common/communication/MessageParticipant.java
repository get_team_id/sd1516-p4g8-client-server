package pt.ua.sd.ropegame.common.communication;

/**
 * Class which contains constants representing each Message participant.
 */
public class MessageParticipant {
    public static final int REFEREE = 10;
    public static final int CONTESTANT = 11;
    public static final int COACH = 12;

    public static final int BENCH = 20;
    public static final int REFSITE = 21;
    public static final int PLAYGROUND = 22;

    public static final int GENREP = 30;
}
